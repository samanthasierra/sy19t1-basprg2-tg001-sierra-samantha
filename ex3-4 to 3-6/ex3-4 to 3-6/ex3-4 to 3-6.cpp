#include <iostream>
#include <string>
#include <time.h>
using namespace std;

//3-1
void arrayPointer(int *numbers, int size)
{
	size = 10;
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 100 + 1;
		cout << numbers[i] << " ";
	}
}

//3-2
int *arrayDynamic(int *numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}
	return numbers;
}

//3-3
int *arrayDelete(int *numbers, int size)
{
	numbers = new int[size];
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}
	return numbers;
}

int main()
{
	//3-1
	const int size = 10;
	int numbers[size];
	srand(time(NULL));
	cout << "3-1: "; arrayPointer(numbers, size);
	cout << endl;
	cout << "3-2: ";

	//3-2
	int *x;
	x = arrayDynamic(numbers, size);
	for (int i = 0; i < size; i++)
	{
		cout << *(x + i) << " ";
	}

	//3-3
	cout << endl;
	cout << "3-3: ";
	int *c;
	c = arrayDelete(numbers, size);
	for (int i = 0; i < size; i++)
	{
		cout << *(c + i) << " ";
	}
	delete[] c;
	c = NULL;
	cout << endl;
	system("pause");
	return 0;
}