#include <iostream>
#include <string>
#include <time.h>
using namespace std;

//ex 2-1 (bet)
void inputBet(int& gold, int& bet)
{
	while (bet < 1 || bet > gold)
	{
		cout << "Gold: " << gold << endl;
		cout << "Bet: ";
		cin >> bet;
		system("cls");
	}
}

//ex 2-2 (dice roll)
int diceRoll(int& dicea, int& diceb)
{
	dicea = rand() % 6 + 1;
	diceb = rand() % 6 + 1;
	return dicea;
	return diceb;
}

//ex 2-3 (payout)
void payout(int presult, int eresult, int bet, int& gold, int pdice1, int pdice2, int edice1, int edice2)
{
	presult = pdice1 + pdice2;
	eresult = edice1 + edice2;
	if (pdice1 == 1 && pdice2 == pdice1)
	{
		if (edice1 == 1 && edice2 == edice1)
		{
			cout << "Player: " << pdice1 << " , " << pdice2 << endl;
			cout << "Enemy: " << edice1 << " , " << edice2 << endl;
			cout << "Draw. Amount of Gold stays the same" << endl;
			gold = gold;
		}
		else if (pdice1 == 1 && pdice2 == pdice1)
		{
			cout << "Player: " << pdice1 << " , " << pdice2 << endl;
			cout << "Enemy: " << edice1 << " , " << edice2 << endl;
			cout << "Snake Eyes. Player wins 3x their bet. " << endl;
			gold = gold + bet * 3;
		}
	}
	else
	{
		if (presult > eresult)
		{
			cout << "Player: " << pdice1 << " , " << pdice2 << " ( " << pdice1 + pdice2 << " ) " << endl;
			cout << "Enemy: " << edice1 << " , " << edice2 << " ( " << edice1 + edice2 << " ) " << endl;
			cout << "Player wins. Player wins their bet." << endl;
			gold = gold + bet;
		}
		else if (presult < eresult)
		{
			cout << "Player: " << pdice1 << " , " << pdice2 << " ( " << pdice1 + pdice2 << " ) " << endl;
			cout << "Enemy: " << edice1 << " , " << edice2 << " ( " << edice1 + edice2 << " ) " << endl;
			cout << "Player loses. Player loses their bet." << endl;
			gold = gold - bet;
		}
		else if (presult == eresult)
		{
			cout << "Player: " << pdice1 << " , " << pdice2 << " ( " << pdice1 + pdice2 << " ) " << endl;
			cout << "Enemy: " << edice1 << " , " << edice2 << " ( " << edice1 + edice2 << " ) " << endl;
			cout << "Draw. Amount of Gold stays the same." << endl;
			gold = gold;
		}
	}
}

//ex 2-4 (play round)
void playRound(int& gold, int& bet)
{
	int pdice1=0, pdice2=0, edice1=0, edice2=0, presult=0, eresult=0;
	inputBet(gold, bet);
	system("cls");
	diceRoll(pdice1, pdice2);
	diceRoll(edice1, edice2);
	payout(presult, eresult, bet, gold, pdice1, pdice2, edice1, edice2);
	cout << "Current Gold: " << gold <<  endl;
}

int main()
{
	srand(time(NULL));
	int bet = 0, gold = 1000;
	while (gold > 0)
	{
		playRound(gold, bet);
		bet = 0;
		system("pause");
		system("cls");
	}
	system("pause");
	return 0;
}