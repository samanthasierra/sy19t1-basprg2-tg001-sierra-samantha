#pragma once
#include <string>
#include <iostream>
using namespace std;

class Shape
{
public:
	Shape();
	~Shape();

	string getName();
	int getNumSides();

	void setName(string name);
	void setNumSides(int numSides);

	virtual int getArea();

private:
	string mName;
	int mNumSides;

};

