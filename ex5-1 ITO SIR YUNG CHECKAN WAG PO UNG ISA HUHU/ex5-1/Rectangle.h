#pragma once
#include "Shape.h"
#include <string>
#include <iostream>
using namespace std;

class Rectangle : public Shape
{
public:
	Rectangle();
	~Rectangle();

	int getLength();
	void setLength(int length);
	int getWidth();
	void setWidth(int width);
	int getArea() override;

private:
	int mLength, mWidth;
};

