#include "Rectangle.h"
using namespace std;

Rectangle::Rectangle()
{
}

Rectangle::~Rectangle()
{
}

int Rectangle::getLength()
{
	return mLength;
}

void Rectangle::setLength(int length)
{
	mLength = length;
}

int Rectangle::getWidth()
{
	return mWidth;
}

void Rectangle::setWidth(int width)
{
	mWidth = width;
}

int Rectangle::getArea()
{
	return mWidth * mLength;
}
