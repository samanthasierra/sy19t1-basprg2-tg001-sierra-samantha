#include <iostream>
#include <string>
#include <vector>
#include "Shape.h"
#include "Square.h"
#include "Rectangle.h"
#include "Circle.h"
using namespace std;

int main()
{
	vector<Shape*> shapes;

	//Square
	Square* square = new Square();
	square->setName("Square");
	square->setNumSides(4);
	square->setLength(5);
	shapes.push_back(square);

    //Rectangle
	Rectangle* rectangle = new Rectangle();
	rectangle->setName("Rectangle");
	rectangle->setNumSides(4);
	rectangle->setLength(6);
	rectangle->setWidth(7);
	shapes.push_back(rectangle);
	
	//Circle
	Circle* circle = new Circle();
	circle->setName("Circle");
	circle->setNumSides(2); 
	circle->setRadius(6);
	shapes.push_back(circle);

	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << shape->getName() << endl;
		cout << "Sides: " << shape->getNumSides() << endl;
		cout << "Area: " << shape->getArea() << endl;
		cout << endl;
	}
	
	system("pause");
	return 0;
}