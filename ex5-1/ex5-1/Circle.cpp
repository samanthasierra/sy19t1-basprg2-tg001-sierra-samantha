#include "Circle.h"
using namespace std;

Circle::Circle()
{
}

Circle::~Circle()
{
}

int Circle::getRadius()
{
	return mRadius;
}

void Circle::setRadius(int radius)
{
	mRadius = radius;
}

int Circle::getArea()
{
	return 3.1416 * (mRadius * mRadius);
}
