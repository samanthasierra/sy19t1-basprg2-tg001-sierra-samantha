#pragma once
#include "Shape.h"
#include <string>
#include <iostream>
using namespace std;

class Circle : public Shape
{
public:
	Circle();
	~Circle();

	int getRadius();
	void setRadius(int radius);
	int getArea() override;

private:
	int mRadius;
};

