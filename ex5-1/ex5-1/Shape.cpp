#include "Shape.h"
using namespace std;

Shape::Shape()
{
}

Shape::~Shape()
{
}

string Shape::getName()
{
	return mName;
}

int Shape::getNumSides()
{
	return mNumSides;
}

void Shape::setName(string name)
{
	mName = name;
}

void Shape::setNumSides(int numSides)
{
	mNumSides = numSides;
}

int Shape::getArea()
{
	return 0;
}
