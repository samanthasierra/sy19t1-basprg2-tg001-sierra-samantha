#pragma once
#include "Shape.h"
#include <string>
#include <iostream>
using namespace std;

class Square : public Shape
{
public:
	Square();
	~Square();

	int getLength();
	void setLength(int value);
	int getArea() override;

private:
	int mLength;
};

