#include<iostream>
#include<string>
#include "Archer.h"

Cloud::Cloud()
{
	this->name = "";
	this->wpn = "";
	this->arm = "";
	this->acc = "";
	this->lvl = 0;
	this->hp = 0;
	this->mp = 0;
	void displayInfo();
}

Cloud::Cloud(string name, string wpn, string arm, string acc, int lvl, int hp, int mp)
{
	this->name = name;
	this->wpn = wpn;
	this->arm = arm;
	this->acc = acc;
	this->lvl = lvl;
	this->hp = hp;
	this->mp = mp;
}

void Cloud::displayInfo()
{
	cout << "Name: " << this->name << endl;
	cout << "Weapon: " << this->wpn << endl;
	cout << "Armor: " << this->arm << endl;
	cout << "Accessory (?): " << this->acc << endl;
	cout << "LVL: " << this->lvl << endl;
	cout << "HP: " << this->hp << endl;
	cout << "MP: " << this->mp << endl;
}