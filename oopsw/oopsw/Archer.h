#pragma once
#include <string>
#include <iostream>

using namespace std;

class Cloud
{
public:
	Cloud(); // default constructor
	Cloud(string name, string wpn, string arm, string acc, int lvl, int hp, int mp); //overload constructor

	string name, wpn, arm, acc;
	int lvl, hp, mp;
	void displayInfo();
};