#include <iostream>
#include <string>
using namespace std;

int findFactorial(int num)
{
	int factorial = 1;
	for (int i = 1; i <= num; i++)
	{
		factorial = factorial * i;
	}
	cout << factorial << endl;
	return factorial;
}
int main()
{
	int num = 0;
	cout << "Input Number: ";
	cin >> num;
	findFactorial(num);
	system("pause");
	return 0;
}
