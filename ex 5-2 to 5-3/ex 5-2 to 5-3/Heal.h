#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;
class Heal : public Unit
{
public:
	Heal();
	~Heal();

	int getHp();
	void setHp(int hp);
	int selfBuff() override;

private:
	int mHp;
};

