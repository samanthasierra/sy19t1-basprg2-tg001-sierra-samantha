#pragma once
#include "Unit.h"
#include <string>
#include <iostream>
using namespace std;

class IronSkin : public Unit
{
public:
	IronSkin();
	~IronSkin();

	int getVitality();
	void setVitality(int vitality);
	int selfBuff() override;

private:
	int mVitality;
};

