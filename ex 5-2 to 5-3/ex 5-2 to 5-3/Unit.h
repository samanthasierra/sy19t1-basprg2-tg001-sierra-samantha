#pragma once
#include <iostream>
#include <string>
using namespace std;

class Unit
{
public:
	Unit();
	~Unit();

	string getName();
	void setName(string name);
	
	virtual int selfBuff();
	
private:
	string mName;
};

