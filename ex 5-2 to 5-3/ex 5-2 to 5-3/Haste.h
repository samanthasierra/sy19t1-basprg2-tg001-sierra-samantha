#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;
class Haste : public Unit
{
public:
	Haste();
	~Haste();

	int getAgility();
	void setAgility(int agility);
	int selfBuff() override;
	
private:
	int mAgility;
};

