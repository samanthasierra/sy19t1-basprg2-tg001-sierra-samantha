#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Concentration.h"
#include "Haste.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Might.h"
using namespace std;

int main()
{
	srand(time(NULL));
	vector<Unit*> units;

	//heal
	Heal* heal = new Heal();
	heal->setName("Heal");
	units.push_back(heal);

	//might
	Might* might = new Might();
	might->setName("Might");
	units.push_back(might);

	//ironskin
	IronSkin* ironskin = new IronSkin();
	ironskin->setName("IronSkin");
	units.push_back(ironskin);

	//concentration
	Concentration* concentration = new Concentration();
	concentration->setName("Concentration");
	units.push_back(concentration);

	//haste
	Haste* haste = new Haste();
	haste->setName("Haste");
	units.push_back(haste);

	int choice;
	
	do
	{
		int randomBuff = rand() % 4 + 1;
		cout << "Continue? (1 - yes; 2 - no) ";
		cin >> choice;

		if (choice != 1)
		{
			system("pause");
		}

		else
		{
			for (int i = 0; i < 1; i++)
			{
				Unit* unit = units[i];
				cout << units[randomBuff]->getName() << endl;

				if (randomBuff == 1)
				{
					cout << "HP + 10. " << endl;
					system("pause");
					system("cls");
				}

				else if (randomBuff == 2)
				{
					cout << "Power + 2." << endl;
					system("pause");
					system("cls");
				}

				else if (randomBuff == 3)
				{
					cout << "Vitality + 2." << endl;
					system("pause");
					system("cls");
				}

				else if (randomBuff == 4)
				{
					cout << "Dexterity + 2." << endl;
					system("pause");
					system("cls");
				}

				else if (randomBuff == 5)
				{
					cout << "Agility + 2." << endl;
					system("pause");
					system("cls");
				}
			}
		}
	}
	while (choice != 2);

	return 0;
}