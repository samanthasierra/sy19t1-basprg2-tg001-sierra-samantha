#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;

class Concentration : public Unit
{
public:
	Concentration();
	~Concentration();

	int getDexterity();
	void setDexterity(int dexterity);
	int selfBuff() override;

private:
	int mDexterity;
};

