#pragma once
#include "Unit.h"
#include <iostream>
#include <string>
using namespace std;
class Might : public Unit
{
public:
	Might();
	~Might();

	int getPower();
	void setPower(int power);
	int selfBuff() override;

private:
	int mPower;
};

