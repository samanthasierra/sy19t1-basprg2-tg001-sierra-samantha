#include <iostream>
#include <string>
#include <time.h>

using namespace std;



int packagesArray(int num, int packages[])
{
	int  sorted;

	for (int i = 0; i <= 6; i++)
	{
		for (int c = i + 1; c <= 6; c++)
		{
			if (packages[i] > packages[c])
			{
				sorted = packages[i];
				packages[i] = packages[c];
				packages[c] = sorted;
			}
		}
	}
	return packages[6];
}


int displayArray (int packages[])
{
	   int count = 1;

	for (int i = 0; i <= 6; i++)
	{
		cout << "Package no: " << count << ": " << packages[i] << endl;
		count++;
	}
	
	return packages[6];

}

int suggestPackage(int packages[], int& input, int& currentGold)
{
	cout << "Input items you want to buy: ";
	cin >> input;
	while (currentGold < 0)
	{
		if (currentGold > 0)
		{
			currentGold = currentGold - input;
			cout << "Item purchased." << endl;
			cout << currentGold << endl;
			currentGold++;
			return currentGold;
		}

		else if (currentGold < 0)
		{
			currentGold = currentGold - input;
			if (input == 150)
			{
				cout << "Package 1 purchased." << endl;
				currentGold = currentGold + 150;
				currentGold++;
				return currentGold;
			}

			else if (input == 500)
			{
				cout << "Package 2 purchased." << endl;
				currentGold = currentGold + 500;
				currentGold++;
				return currentGold;
			}

			else if (input == 1780)
			{
				cout << "Package 3 purchased." << endl;
				currentGold = currentGold + 1780;
				currentGold++;
				return currentGold;
			}

			else if (input == 4050)
			{
				cout << "Package 4 purchased." << endl;
				currentGold = currentGold + 4050;
				currentGold++;
				return currentGold;
			}

			else if (input == 13333)
			{
				cout << "Package 5 purchased." << endl;
				currentGold = currentGold + 13333;
				currentGold++;
				return currentGold;
			}

			else if (input == 30750)
			{
				cout << "Package 6 purchased." << endl;
				currentGold = currentGold + 30750;
				currentGold++;
				return currentGold;
			}

			else if (input == 250000)
			{
				cout << "Package 7 purchased." << endl;
				currentGold = currentGold + 250000;
				currentGold++;
				return currentGold;
			}

			
		}
		
		currentGold--;
		int exit = 0;
		
	}
	return currentGold;
}


void exitProgram(int exit, int packages[], int& input, int& currentGold)
{
	cout << "Continue? (1 = yes, 2 = no)" << endl;
	cin >> exit;
	if (exit == 2)
	{
		cout << "Ok." << endl;

	}

	else if (exit == 1)
	{
		displayArray(packages);
		suggestPackage(packages, input, currentGold);

	}
}



int main()
{
	int num=0, input, exit = 0;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int currentGold = 250;
	packagesArray(num, packages);
	displayArray(packages);
	cout << "Current Gold: " << currentGold << endl;
	cout << suggestPackage(packages, input, currentGold);
	exitProgram(exit, packages, input, currentGold);
	

	system("pause");
	return 0;

}
