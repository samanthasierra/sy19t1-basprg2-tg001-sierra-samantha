#include "Enemy.h"
#include "Character.h"
#include <time.h>

Enemy::Enemy()
{
	this->eName = " ";
}

Enemy::Enemy(string eName, int eMinHp, int eMaxHp, int eMinPow, int eMaxPow, int eMinVit, int eMaxVit, int eMinDex, int eMaxDex, int eMinAgi, int eMaxAgi)
{
	this->eName = eName;
	mEMinHp = eMinHp;
	mEMaxHp = eMaxHp;
	mEMinPow = eMinPow;
	mEMaxPow = eMaxPow;
	mEMinVit = eMinVit;
	mEMaxVit = eMaxVit;
	mEMinDex = eMinDex;
	mEMaxDex = eMaxDex;
	mEMinAgi = eMinAgi;
	mEMaxAgi = eMaxAgi;
	mEhp = eHp;
	mePow = ePow;
	mEvit = eVit;
	mEdex = eDex;
	mEagi = eAgi;
}

Enemy::~Enemy()
{
}

string Enemy::getEname()
{
	return eName;
}

int Enemy::getEstats()
{
	srand(time(NULL));
	this->eName = eName;
	mEhp = rand() % (mEMinHp - mEMaxHp + 1) + 1;
	mePow = rand() % (mEMinPow - mEMaxPow + 1) + 1;
	mEvit = rand() % (mEMinVit - mEMaxVit + 1) + 1;
	mEdex = rand() % (mEMinDex - mEMaxDex + 1) + 1;
	mEagi = rand() % (mEMinAgi - mEMaxAgi + 1) + 1;

	return eName, mEhp, mePow, mEvit, mEdex, mEagi, mEMinHp, mEMinPow, mEMinVit, mEMinDex, mEMinAgi, mEMaxHp, mEMaxPow, mEMaxVit, mEMaxDex, mEMaxAgi;
}

void Enemy::displayEstats()
{
	int eHp = rand() % (mEMinHp - mEMaxHp + 1);
	int ePow = rand() % (mEMinPow - mEMaxPow + 1);
	int eVit = rand() % (mEMinVit - mEMaxVit + 1);
	int eDex = rand() % (mEMinDex - mEMaxDex + 1);
	int eAgi = rand() % (mEMinAgi - mEMaxAgi + 1);

	cout << "Name: " << eName << endl;
	cout << "Hp: " << eHp << endl;
	cout << "Pow: " << ePow << endl;
	cout << "Vit: " << eVit << endl;
	cout << "Dex: " << eDex << endl;
	cout << "Agi: " << eAgi << endl;
}


