#pragma once
#include <iostream>
#include <string>
#include "Enemy.h"
#include "Character.h"

using namespace std;
class Character
{
public:
	
	Character();
	Character(string name, string pclass, int minHp, int maxHp, int minPow, int maxPow, int minVit, int maxVit, int minDex, int maxDex, int minAgi, int maxAgi);
	~Character();

	string name, pclass;
	int minhp, maxhp, minpow, maxpow, minvit, maxvit, mindex, maxdex, minagi, maxagi;
	int hp, pow, vit, dex, agi;
	string getName();
	
	int getStats();
	void displayStats();
	
private:
	int mHp, mPow, mVit, mDex, mAgi, mminHp, mminPow, mminVit, mminDex, mminAgi, mmaxHp, mmaxPow, mmaxVit, mmaxDex, mmaxAgi;
	
};
