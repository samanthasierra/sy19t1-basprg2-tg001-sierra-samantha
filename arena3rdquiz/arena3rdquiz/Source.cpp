#include <iostream>
#include <string>
#include <time.h>
#include "Character.h"
#include "Enemy.h"
using namespace std;

int randEnemy()
{
	int character = rand() % 7;
	return character;
}

int changeTurns()
{
	int turn = rand() % 3;
	if (turn == 1)
	{
		cout << "Player fights first." << endl;
	}
	else if (turn == 2)
	{
		cout << "Enemy fights first." << endl;
	}
	return turn;
}

int main()
{
	string name;
	int pclass, player, enemy;

	//Input name of player
	cout << "Name of player: ";
	cin >> name;
	cout << endl;
	system("cls");

	//classes
	Character* Warrior = new Character(name, "Warrior", 3, 30, 3, 20, 2, 15, 2, 15, 2, 15);
	Character* Assassin = new Character(name, "Assassin", 3, 30, 3, 20, 2, 15, 2, 15, 2, 15);
	Character* Mage = new Character(name, "Mage", 3, 30, 2, 20, 3, 15, 2, 15, 2, 15);
	
	Enemy* Warrior2 = new Enemy("Enemy Warrior", 1, 20, 1, 10, 1, 10, 1, 10, 1, 10);
	Enemy* Assassin2 = new Enemy("Enemy Assassin", 1, 20, 1, 10, 1, 10, 1, 10, 1, 10);
	Enemy* Mage2 = new Enemy("Enemy Mage", 1, 20, 1, 10, 1, 10, 1, 10, 1, 10);

	//choose class of player
	
	cout << "Choose class:" << endl;
	cout << "1 - Warrior; 2 - Assassin; 3 - Mage" << endl;
	cin >> pclass;
	system("cls");

	//display player information
	if (pclass == 1)
	{
		Warrior->getName();
		Warrior->getStats();
		Warrior->displayStats();
	}
	else if (pclass == 2)
	{
		Assassin->getName();
		Assassin->getStats();
		Assassin->displayStats();
	}
	else if (pclass == 3)
	{
		Mage->getName();
		Mage->getStats();
		Mage->displayStats();
	}

	cout << endl;
	cout << "vs" << endl;
	cout << endl;

	//display enemy information
	if (randEnemy() == 1)
	{
		Warrior2->getEname();
		Warrior2->getEstats();
		Warrior2->displayEstats();
	}
	else if (randEnemy() == 2)
	{
		Assassin2->getEname();
		Assassin2->getEstats();
		Assassin2->displayEstats();
	}
	else if (randEnemy() == 3)
	{
        Mage2->getEname();
		Mage2->getEstats();
		Mage2->displayEstats();
	}

	system("pause");
	system("cls");

	changeTurns();

	system("pause");
	system("cls");

	// combat
	do
	{
		switch (pclass)
		{
		case 1: // Warrior vs Enemy Warrior

			if (changeTurns() == 1) // Warrior attacks Enemy Warrior
			{
				int hitRate = (Warrior->dex / Warrior2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Warrior->pow - Warrior2->eVit;
					cout << "Player attacks Enemy Warrior." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Warrior2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 2) // Enemy Warrior attacks Warrior
			{
				int hitRate = (Warrior2->eDex / Warrior->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Warrior2->ePow - Warrior->vit;
					cout << "Enemy Warrior attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Warrior->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 3) // Warrior attacks Enemy Assassin
			{
				int hitRate = (Warrior2->eDex / Warrior->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Warrior->pow - Assassin2->eVit) * 1.5;
					cout << "Player attacks Enemy Assassin" << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Assassin2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 4) // Enemy Assassin attacks Warrior
			{
				int hitRate = (Assassin2->eDex / Warrior->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Assassin2->ePow - Warrior->vit;
					cout << "Enemy Assassin attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Warrior->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 5) // Warrior attacks Mage
			{
				int hitRate = (Warrior->dex / Mage2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Warrior->pow - Mage2->eVit;
					cout << "Player attacks Enemy Mage." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Mage2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 6) // Enemy Mage attacks Warrior
			{
				int hitRate = (Mage2->eDex / Warrior->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Mage2->ePow - Warrior->vit) * 1.5;
					cout << "Enemy Mage attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Warrior->hp -= damage;
					system("pause");
				}
			}
			break;

		case 2: // Assassin vs Enemy Assassin

			if (changeTurns() == 1) // Assassin attacks Enemy Assassin
			{
				int hitRate = (Assassin->dex - Assassin2->eVit) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Assassin->pow - Assassin2->eVit;
					cout << "Player attacks Enemy Assassin." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Assassin2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 2) // Enemy Assassin attacks Assassin
			{
				int hitRate = (Assassin2->eDex - Assassin->vit) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Assassin2->ePow - Assassin->pow;
					cout << "Enemy Assassin attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Assassin->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 3) // Assassin attacks Enemy Warrior
			{
				int hitRate = (Assassin->dex / Warrior2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Assassin->pow - Warrior2->eVit);
					cout << "Assassin attacks Enemy Warrior." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Warrior2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 4) // Enemy Warrior attacks Assassin
			{
				int hitRate = (Warrior2->eDex / Assassin->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Warrior2->ePow - Assassin->vit) * 1.5;
					cout << "Enemy Warrior attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Assassin->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 5) // Assassin attacks Enemy Mage
			{
				int hitRate = (Assassin->dex / Mage2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Assassin->pow - Mage2->eVit) * 1.5;
					cout << "Player attacks Enemy Mage." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Mage2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 6) // Enemy Mage attacks Assassin
			{
				int hitRate = (Mage2->eDex / Assassin->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Mage2->ePow - Assassin->vit) * 1.5;
					cout << "Enemy Mage attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Assassin->hp -= damage;
					system("pause");
				}
			}


			break;

		case 3: 

			if (changeTurns() == 1) // Mage attacks Enemy MAge
			{
				int hitRate = (Mage->dex - Mage2->eVit) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Mage->pow - Mage2->eVit;
					cout << "Player attacks Enemy Mage." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Mage2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 2) // Enemy Mage attacks Mage
			{
				int hitRate = (Mage2->eDex - Mage->vit) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = Mage2->ePow - Mage->pow;
					cout << "Enemy Mage attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Mage->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 3) // Mage attacks Enemy Warrior
			{
				int hitRate = (Mage->dex / Warrior2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Mage->pow - Warrior2->eVit);
					cout << "Player attacks Enemy Warrior." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Warrior2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 4) // Enemy Warrior attacks Mage
			{
				int hitRate = (Warrior2->eDex / Mage->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Warrior2->ePow - Mage->vit) * 1.5;
					cout << "Enemy Warrior attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Mage->hp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 5) // Mage attacks Enemy Assassin 
			{
				int hitRate = (Mage->dex / Assassin2->eAgi) * 100;
				if (hitRate <= 45)
				{
					cout << "Player missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Mage->pow - Assassin2->eVit);
					cout << "Player attacks Enemy Assassin." << endl;
					cout << "Damage given to enemy: " << damage << endl;
					Assassin2->eHp -= damage;
					system("pause");
				}
			}
			else if (changeTurns() == 6) // Enemy Assassin attacks Mage
			{
				int hitRate = (Assassin2->eDex / Mage->agi) * 100;
				if (hitRate <= 45)
				{
					cout << "Enemy missed." << endl;
					system("pause");
				}
				else if (hitRate > 45)
				{
					int damage = (Assassin2->ePow - Mage->vit) * 1.5;
					cout << "Enemy Assassin attacks Player." << endl;
					cout << "Damage given to player: " << damage << endl;
					Mage->hp -= damage;
					system("pause");
				}
			}
			break;

		default:
		{
			break;
		}

		}

		//result
		if (Warrior->hp <= 0)
		{
			cout << Warrior->name << " loses." << endl;
		}
		else if (Warrior2->eHp <= 0)
		{
			cout << Warrior->name << " wins." << endl;
			Warrior->hp + 3 + (0.3 * Warrior->hp);
			Warrior->vit + 3;
		}
		else if (Assassin2->eHp <= 0)
		{
			cout << Warrior->name << " wins." << endl;
			Warrior->hp + 3 + (0.3 * Warrior->hp);
			Warrior->vit + 3;
		}
		else if (Mage2->eHp <= 0)
		{
			cout << Warrior->name << " wins." << endl;
			Warrior->hp + 3 + (0.3 * Warrior->hp);
			Warrior->vit + 3;
		}

		else if (Assassin->hp <= 0)
		{
			cout << Assassin->name << " loses." << endl;
		}
		else if (Assassin2->eHp <= 0)
		{
			cout << Assassin->name << " wins." << endl;
			Assassin->agi + 3;
			Assassin->dex + 3;
			Assassin->hp + (0.3 * Assassin->hp);
		}
		else if (Warrior2->eHp <= 0)
		{
			cout << Assassin->name << " wins." << endl;
			Assassin->agi + 3;
			Assassin->dex + 3;
			Assassin->hp + (0.3 * Assassin->hp);
		}
		else if (Mage2->eHp <= 0)
		{
			cout << Assassin->name << " wins." << endl;
			Assassin->agi + 3;
			Assassin->dex + 3;
			Assassin->hp + (0.3 * Assassin->hp);
		}

		else if (Mage->hp <= 0)
		{
			cout << Mage->name << " loses." << endl;
		}
		else if (Mage2->eHp <= 0)
		{
			cout << Mage->name << " wins." << endl;
			Mage->pow + 5;
			Mage->hp + (0.3 * Mage->hp);
		}
		else if (Warrior2->eHp <= 0)
		{
			cout << Mage->name << " wins." << endl;
			Mage->pow + 5;
			Mage->hp + (0.3 * Mage->hp);
		}
		else if (Assassin2->eHp <= 0)
		{
			cout << Mage->name << " wins." << endl;
			Mage->pow + 5;
			Mage->hp + (0.3 * Mage->hp);
		}

	}
	while (Warrior->hp > 0 && Warrior2->eHp > 0 && Assassin->hp > 0 && Assassin2->eHp > 0 && Mage->hp > 0 && Mage2->eHp > 0);

	delete Warrior, Assassin, Mage;
	delete Warrior2, Assassin2, Mage2;

	system("pause");
	return 0;
}
