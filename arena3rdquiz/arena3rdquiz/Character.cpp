#include "Character.h"
#include "Enemy.h"
#include <time.h>

Character::Character()
{
	this->name=" ";
	this->pclass = " ";
}

Character::Character(string name, string pclass, int minHp, int maxHp, int minPow, int maxPow, int minVit, int maxVit, int minDex, int maxDex, int minAgi, int maxAgi)
{
	this->name = name;
	this->pclass = pclass;
	mminHp = minHp;
	mmaxHp = maxHp;
	mminPow = minPow;
	mmaxPow = maxPow;
	mminVit = minVit;
	mmaxVit = maxVit;
	mminDex = minDex;
	mmaxDex = maxDex;
	mminAgi = minAgi;
	mmaxAgi = maxAgi;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}

Character::~Character()
{
}

string Character::getName()
{
	return name;
}

int Character::getStats()
{
	srand(time(NULL));
	this->name = name;
	this->pclass = pclass;
	mHp = rand() % (mminHp - mmaxHp + 1);
	mPow = rand() % (mminPow - mmaxPow + 1);
	mVit = rand() % (mminVit - mmaxVit + 1);
	mDex = rand() % (mminDex - mmaxDex + 1);
	mAgi = rand() % (mminAgi - mmaxAgi + 1);

	return name, mHp, mPow, mVit, mDex, mAgi, mminHp, mminPow, mminVit, mminDex, mminAgi, mmaxHp, mmaxPow, mmaxVit, mmaxDex, mmaxAgi;
}

void Character::displayStats()
{
	
	int hp = rand() % (mminHp - mmaxHp + 1) + 1;
	int pow = rand() % (mminPow - mmaxPow + 1) + 1;
	int vit = rand() % (mminVit - mmaxVit + 1) + 1;
	int dex = rand() % (mminDex - mmaxDex + 1) + 1;
	int agi = rand() % (mminAgi - mmaxAgi + 1)+ 1;

	cout << "Name: " << name << endl;
	cout << "Class: " << pclass << endl;
	cout << "Hp: " << hp << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}

