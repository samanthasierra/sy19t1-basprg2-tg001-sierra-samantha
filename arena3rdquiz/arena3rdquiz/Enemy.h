#pragma once
#include <iostream>
#include <string>
#include "Character.h"
#include "Enemy.h"

using namespace std;
class Enemy
{
public:

	Enemy();
	Enemy(string eName, int eMinHp, int eMaxHp, int eMinPow, int eMaxPow, int eMinVit, int eMaxVit, int eMinDex, int eMaxDex, int eMinAgi, int eMaxAgi);
	~Enemy();

	string eName; 
	int eMinHp, eMaxHp, eMinPow, eMaxPow, eMinVit, eMaxVit, eMinDex, eMaxDex, eMinAgi, eMaxAgi;
	int eHp, ePow, eVit, eDex, eAgi;

	string getEname();
	int getEstats();
	void displayEstats();

private:
	int mEhp, mePow, mEvit, mEdex, mEagi, mEMinHp, mEMinPow, mEMinVit, mEMinDex, mEMinAgi, mEMaxHp, mEMaxPow, mEMaxVit, mEMaxDex, mEMaxAgi;
	
};
