#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;
class Bomb : public Item
{
public:
	Bomb();
	~Bomb();

	int getHp();
	void setHp(int hp);

	int itemEffect(int target) override;

private:
	int mHp;
};

