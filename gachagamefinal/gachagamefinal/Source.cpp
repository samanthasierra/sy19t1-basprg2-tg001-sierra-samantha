#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Item.h"
#include "Bomb.h"
#include "GetCrystals.h"
#include "HealthPotion.h"
#include "ItemR.h"
#include "ItemSR.h"
#include "ItemSSR.h"
using namespace std;

int startingHp = 100, startingCrystal = 100, startingRarityPoints = 0, crystalCost = 5; //starting player information

int main()
{
	srand(time(NULL));

	vector<Item*> items;

	// classes needed
	Bomb* bomb = new Bomb();
	bomb->setName("Bomb");
	bomb->setHp(0);
	bomb->getHp();
	items.push_back(bomb);

	GetCrystals* crystal = new GetCrystals();
	crystal->setName("Crystals");
	crystal->setBonusCrystal(0);
	crystal->getBonusCrystal();
	items.push_back(crystal);

	HealthPotion* healthPotion = new HealthPotion();
	healthPotion->setName("Health Potion");
	healthPotion->setHp(0);
	healthPotion->getHp();
	items.push_back(healthPotion);

	ItemR* itemR = new ItemR();
	itemR->setName("R");
	itemR->setRarityPoint(0);
	itemR->getRarityPoint();
	items.push_back(itemR);

	ItemSR* itemSR = new ItemSR();
	itemSR->setName("SR");
	itemSR->setRarityPoint(0);
	itemSR->getRarityPoint();
	items.push_back(itemSR);

	ItemSSR* itemSSR = new ItemSSR();
	itemSSR->setName("SSR");
	itemSSR->setRarityPoint(0);
	itemSSR->getRarityPoint();
	items.push_back(itemSSR);

	int pulls = 0, totalCrystal = 0, totalR = 0, totalBomb = 0, totalHealthPotion = 0, totalSR = 0, totalSSR = 0;

	do
	{
		int random = rand() % 100 + 1; //randomize items

		for (int i = 0; i < 1; i++)
		{
			cout << "HP: " << startingHp << endl;
			cout << "Crystals: " << startingCrystal << endl;
			cout << "Rarity Points: " << startingRarityPoints << endl;
			cout << "Pulls: " << pulls << endl;
			cout << endl;

			startingCrystal = startingCrystal - 5; //each pull costs 5 crystals

			if (random <= 15) //15% healthpotion
			{
				cout << "You pulled " << healthPotion->getName() << "." << endl;
				cout << "You are healed for 30 HP." << endl;
				startingHp = startingHp + healthPotion->itemEffect(startingHp);
				cout << endl;
				totalHealthPotion++;
			}

			else if (random <= 30) //15% crystal
			{
				cout << "You pulled " << crystal->getName() << "." << endl;
				cout << "You received 15 crystals." << endl;
				startingCrystal = startingCrystal + crystal->itemEffect(startingCrystal);
				cout << endl;
				totalCrystal++;
			}

			else if (random <= 70) //40% r
			{
				cout << "You pulled " << itemR->getName() << "." << endl;
				cout << "You received 1 Rarity Point." << endl;
				startingRarityPoints = startingRarityPoints + itemR->itemEffect(startingRarityPoints);
				cout << endl;
				totalR++;
			}

			else if (random <= 79) //9% sr 
			{
				cout << "You pulled " << itemSR->getName() << "." << endl;
				cout << "You received 10 Rarity Points." << endl;
				startingRarityPoints = startingRarityPoints + itemSR->itemEffect(startingRarityPoints);
				cout << endl;
				totalSR++;
			}

			else if (random <= 80) //1% ssr
			{
				cout << "You pulled " << itemSSR->getName() << "." << endl;
				cout << "You received 50 Rarity Points." << endl;
				startingRarityPoints = startingRarityPoints + itemSSR->itemEffect(startingRarityPoints);
				cout << endl;
				totalSSR++;
			}
			else  //20% bomb
			{
				cout << "You pulled " << bomb->getName() << "." << endl;
				cout << "You received 25 damage." << endl;
				startingHp = startingHp + bomb->itemEffect(startingHp);
				cout << endl;
				totalBomb++;
			}
			pulls++;
			system("pause");
			system("cls");
		}

	} while (startingHp > 0 && startingCrystal > 0 && startingRarityPoints < 100);

	//prevent negative numbers
	if (startingCrystal < 0)
	{
		startingCrystal = 0;
	}
	if (startingHp < 0)
	{
		startingHp = 0;
	}

	if (startingHp > 0 && startingCrystal > 0 && startingRarityPoints >= 100) //win
	{
		cout << "You won!" << endl;
		cout << endl;
	}

	else if (startingHp <= 0 || startingCrystal <= 0) //lose
	{
		cout << "You lost!" << endl;
		cout << endl;
	}

	//game result/summary
	cout << "===========================================" << endl;
	cout << "HP: " << startingHp << endl;
	cout << "Crystals: " << startingCrystal << endl;
	cout << "Rarity Points: " << startingRarityPoints << endl;
	cout << "Pulls: " << pulls - 1 << endl;
	cout << "===========================================" << endl;
	cout << endl;

	cout << "Items Pulled:" << endl;
	cout << "------------------------------------------" << endl;
	cout << "R: x" << totalR << endl;
	cout << "SR: x" << totalSR << endl;
	cout << "SSR: x" << totalSSR << endl;
	cout << "Health Potion: x" << totalHealthPotion << endl;
	cout << "Bomb: x" << totalBomb << endl;
	cout << "Crystals: x" << totalCrystal << endl;

	system("pause");
	return 0;
}