#include "ItemR.h"

ItemR::ItemR()
{
}

ItemR::~ItemR()
{
}

int ItemR::getRarityPoint()
{
	return mRarityPoint;
}

void ItemR::setRarityPoint(int rarityPoint)
{
	mRarityPoint = rarityPoint;
}

int ItemR::itemEffect(int target)
{
	return mRarityPoint + 1;
}
