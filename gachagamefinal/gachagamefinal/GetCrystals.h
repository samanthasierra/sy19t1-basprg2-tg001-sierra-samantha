#pragma once
#include <string>
#include <iostream>
#include "Item.h"
using namespace std;
class GetCrystals : public Item
{
public:
	GetCrystals();
	~GetCrystals();

	int getBonusCrystal();
	void setBonusCrystal(int crystal);

	int itemEffect(int target) override;

private:
	int mCrystal;
};

