#include "HealthPotion.h"

HealthPotion::HealthPotion()
{
}

HealthPotion::~HealthPotion()
{
}

int HealthPotion::getHp()
{
	return mHp;
}

void HealthPotion::setHp(int hp)
{
	mHp = hp;
}

int HealthPotion::itemEffect(int target)
{
	return mHp + 30;
}
