#pragma once
#include <iostream>
#include <string>
using namespace std;
class Item
{
public:
	Item();
	~Item();

	string getName();
	string getItemType();

	void setName(string name);

	virtual int itemEffect(int target);

private:
	string mName, mItemType;
};

