#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;
class ItemSR : public Item
{
public:
	ItemSR();
	~ItemSR();

	int getRarityPoint();
	void setRarityPoint(int rarityPoint);

	int itemEffect(int target) override;

private:
	int mRarityPoint;
};

