#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;
class HealthPotion : public Item
{
public:
	HealthPotion();
	~HealthPotion();

	int getHp();
	void setHp(int hp);

	int itemEffect(int target) override;

private:
	int mHp;
};

