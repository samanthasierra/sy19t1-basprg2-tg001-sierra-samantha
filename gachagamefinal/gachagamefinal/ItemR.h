#pragma once
#include <iostream>
#include <string>
#include "Item.h"
using namespace std;
class ItemR : public Item
{
public:
	ItemR();
	~ItemR();

	int getRarityPoint();
	void setRarityPoint(int rarityPoint);

	int itemEffect(int target) override;

private:
	int mRarityPoint;
};
